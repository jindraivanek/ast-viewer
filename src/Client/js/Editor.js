import React, {useRef} from 'react';
import {ControlledEditor as MonacoEditor} from '@monaco-editor/react';
import PropTypes from 'prop-types';
import ReactResizeDetector from 'react-resize-detector';

const Editor = ({onChange, language, getEditor, value, isReadOnly}) => {
    const editorRef = useRef();

    const options = {
        readOnly: isReadOnly,
        selectOnLineNumbers: true,
        lineNumbers: true,
        theme: 'vs-light',
        renderWhitespace: "all",
        minimap: {
            enabled: false
        }
    };
    const handleEditorChange = (ev, value) => {
        onChange(value);
    };

    function handleEditorDidMount(kel, editor) {
        editorRef.current = editor;
        getEditor(editor);
    }

    function onResize() {
        if (editorRef && editorRef.current) {
            editorRef.current.layout();
        }
    }

    return <div style={{height: '70vh', overflow: 'hidden'}}>
        <ReactResizeDetector handleWidth handleHeight onResize={onResize}/>
        <MonacoEditor height={"70vh"} language={language} editorDidMount={handleEditorDidMount} value={value}
                      onChange={handleEditorChange}
                      options={options}/>
    </div>
}

function noop() {
}

Editor.propTypes = {
    onChange: PropTypes.func,
    value: PropTypes.string,
    language: PropTypes.string,
    isReadOnly: PropTypes.bool,
    getEditor: PropTypes.func
};

Editor.defaultProps = {
    onChange: noop,
    value: "",
    language: "fsharp",
    isReadOnly: false,
    getEditor: noop
};

export default Editor;