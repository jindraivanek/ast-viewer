FROM nojaf/fable:2.5 AS builder
SHELL ["/bin/bash", "-c"]

WORKDIR /build

COPY .config .config
RUN dotnet tool restore

COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile
COPY paket.dependencies paket.lock ./
RUN dotnet paket restore
COPY src src
COPY build.fsx ./
RUN rm -rf deploy

# Build client files
RUN dotnet fake run build.fsx --target Bundle
RUN cp -r deploy/Client/public public

# Build server
RUN fake run build.fsx --target BuildServer
RUN pwsh -c "Compress-Archive ./deploy/Server/* ./deploy/Server.zip"
