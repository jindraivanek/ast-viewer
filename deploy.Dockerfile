FROM nojaf/fable:2.5 AS builder
SHELL ["/bin/bash", "-c"]

WORKDIR /build

COPY .config .config
RUN dotnet tool restore

COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile
COPY paket.dependencies paket.lock ./
RUN dotnet paket restore
COPY src src
COPY build.fsx ./
RUN rm -rf deploy

# Build client files
RUN dotnet fake run build.fsx --target Bundle
RUN cp -r deploy/Client/public public

# Build server
RUN fake run build.fsx --target BuildServer
RUN pwsh -c "Compress-Archive ./deploy/Server/* ./deploy/Server.zip"

# Deploy server
FROM mcr.microsoft.com/azure-cli
WORKDIR /deploy

ARG AZ_USER
ENV AZ_USER=${AZ_USER}
ARG AZ_PASS
ENV AZ_PASS=${AZ_PASS}
ARG AZ_TENANT
ENV AZ_TENANT=${AZ_TENANT}
ARG AZ_RESOURCEGROUP
ENV AZ_RESOURCEGROUP=${AZ_RESOURCEGROUP}
ARG AZ_FUNCTIONNAME
ENV AZ_FUNCTIONNAME=${AZ_FUNCTIONNAME}

COPY --from=builder /build/deploy/Server.zip ./
COPY --from=builder /build/public/ ./build/public/
RUN if [ "x$AZ_USER" != "x" ] && [ "x$AZ_PASS" != "x" ] && [ "x$AZ_TENANT" != "x" ]; then az login --service-principal --username $AZ_USER --password $AZ_PASS --tenant $AZ_TENANT; fi
# See https://markheath.net/post/deploying-azure-functions-with-azure-cli
RUN if [ "x$AZ_RESOURCEGROUP" != "x" ] && [ "x$AZ_FUNCTIONNAME" != "x" ]; then az functionapp deployment source config-zip -g $AZ_RESOURCEGROUP -n $AZ_FUNCTIONNAME --src ./Server.zip ; fi